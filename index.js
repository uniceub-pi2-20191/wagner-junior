/**
 * @format
 */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

var RCTLog = require('RCTLog');

AppRegistry.registerComponent(appName, () => App);
