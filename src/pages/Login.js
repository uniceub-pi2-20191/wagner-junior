import React, {Component} from 'react';
import {Alert, Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Actions} from 'react-native-router-flux'
import Logo from '../components/Logo'
import Form from '../components/Form'

export default class Login extends Component{

  

  signup(){
     Actions.signup()
  }

    render(){
      return(
          <View style={styles.container}>
              <Logo />
              <Form  type='Login'/>
              <View style={styles.singupTextCont}>
                  <Text style={styles.signupText}>Não tem uma conta ainda?</Text>
                  <TouchableOpacity onPress={this.signup}><Text style={styles.signupBotton}> Cadastre-se!</Text></TouchableOpacity>
              </View>
          </View>
      )
   }
}
       
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#34495e',
    },
    singupTextCont: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    signupText: {
      color: '#95a5a6'
    },
    signupBotton: {
      color: '#e74c3c',
      
      fontWeight: '500'
    }
  });
       
    
    

    