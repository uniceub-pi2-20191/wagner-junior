import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, TouchableOpacity} from 'react-native';
import {Actions} from 'react-native-router-flux'
import Logo from '../components/Logo'
import Form from '../components/Form'

export default class Signup extends Component{

goBack(){
  Actions.pop()
}
    render(){
      return(
          <View style={styles.container}>
              <Logo />
              <Form type='Cadastrar'/>
              <View style={styles.singupTextCont}>
                 <Text style={styles.signupText}>Já tem uma conta?</Text>
                  <TouchableOpacity onPress={this.goBack}><Text style={styles.signupBotton}> Logar!</Text></TouchableOpacity>
              </View>
          </View>
      )
   }
}
       
const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#34495e',
    },
    singupTextCont: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      flexDirection: 'row'
    },
    signupText: {
      color: '#95a5a6'
    },
    signupBotton: {
      color: '#e74c3c',
      fontSize: 16,
      fontWeight: '500'
    }
})   