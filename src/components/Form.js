import React, {Component} from 'react';
import {Alert, Platform, StyleSheet, Text, View, Image, TextInput, TouchableOpacity} from 'react-native';

export default class Form extends Component{
  onPressEvent(){
    Alert.alert('Logado com sucesso!')
 }
    
    render(){
      return(
              <View style={styles.container}>
                 <TextInput style={styles.inputBox} 
                 placeholder='usuário ou e-mail'
                 placeholderTextColor='rgba(255,255,255,0.7)'
                 keyboardType='email-address'
                 onSubmitEditing={() => this.password.focus()}/>

                <TextInput style={styles.inputBox} 
                 placeholder='senha'
                 placeholderTextColor='rgba(255,255,255,0.7)'
                 secureTextEntry={true}
                 ref={(input) => this.password = input}/>

                 <TouchableOpacity onPress = {this.onPressEvent} style={styles.buttonContainer}>
                     <Text style={styles.buttonText}>{this.props.type}</Text>
                 </TouchableOpacity>
              </View>
              )
   }
}

const styles = StyleSheet.create({
  container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center'
  },
  inputBox: {
    width: 300,
    backgroundColor: 'rgba(255,255,255,0.2)',
    marginBottom: 20,
    color: '#FFF',
    paddingHorizontal: 10
  },
  buttonText: {
    textAlign: 'center',
    color: '#34495e',
    fontWeight: '700'
},
buttonContainer: {
    width: 300,
    borderRadius: 25,
    backgroundColor: '#ecf0f1',
    paddingVertical: 10
}
});